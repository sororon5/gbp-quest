class Skill():
	def __init__(self, name, type):
		self.name = name
		self.type = type

class Attack(Skill):
	def __init__(self, name, type, power, acc):
		super().__init__(name, type)
		self.power = power
		self.acc = acc

class Nonattack(Skill):
	def __init__(self, name, type, value):
		super().__init__(name, type)
		self.value = value

TYPE_DICT = {
	"atk": "<攻撃>", 
	"buf": "<強化>", 
	"sup": "<支援>", 
	"dbf": "<妨害>", 
	"rec": "<回復>", 
	"hel": "<治癒>", 
}

# 攻撃
ATK_DICT = {
	"punch": Attack("パンチ", TYPE_DICT["atk"], 50, 100),
	"kick": Attack("キック", TYPE_DICT["atk"], 100, 100),
	"head": Attack("頭突き", TYPE_DICT["atk"], 200, 100),
}

# 強化
BUF_DICT = {
	"con": Nonattack("集中", TYPE_DICT["buf"], 100),
}

# 支援
SUP_DICT = {
	"cheer": Nonattack("応援", TYPE_DICT["sup"], 100),
}

# 妨害
DBF_DICT = {
	"glare": Nonattack("睨む", TYPE_DICT["dbf"], 100),
}

# 回復
REC_DICT = {
	"renew": Nonattack("自己再生", TYPE_DICT["rec"], 100),
}

# 治癒
HEL_DICT = {
	"heal": Nonattack("回復魔法", TYPE_DICT["hel"], 100),
}

skills = {
	"atk": ATK_DICT,
	"buf": BUF_DICT,
	"sup": SUP_DICT,
	"dbf": DBF_DICT,
	"rec": REC_DICT,
	"hel": HEL_DICT,
}
