import selection
import data
import quest

# クエストナンバー
q_num = 1
# パーティーメンバー
p_member = 3

# キャラクター選択、partyに格納
party = []
selection.set_party(party, data.CHARA_LIST, p_member)

# 速さ順に並び替え、p_orderに格納
p_order = quest.sort_list(party, "spd")

# 敵キャラクターをe_orderに格納
enemy = []

# クエスト開始
quest.quest(q_num, p_order, selection.set_enemy(q_num, enemy))

print("complete!")
q_num = 2
quest.quest(q_num, p_order, selection.set_enemy(q_num, enemy))
